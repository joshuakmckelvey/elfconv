PROG=uprint

all : program

# Program is loaded to L2 cache
program : elfconv.x
	./elfconv.x --infile tests/$(PROG).x --split 0 --stride 1 --wordlen 4 --tagbits 18 --filesize 16384 --outfile l2_init

elfconv.x : elfconv.c
	gcc -Wall elfconv.c -o elfconv.x

clean :
	rm -f *.dat *.x
