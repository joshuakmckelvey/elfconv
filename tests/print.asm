	.option nopic
    .data
    .section	.rodata
	.align	2
.LC0:
	.string	"Hello World!"

	.text
	.align	2
	.globl	main
    .globl _start
	.type	main, @function
_start:
main:
	li a0, 4	# Loads print_s syscall code
	la a1, .LC0 # Address of string
	ecall		# Call print_s
	li a0, 10	# Loads exit syscall code
	ecall		# Call exit
	nop		# Padding
	.size	main, .-main
	.section	.note.GNU-stack,"",@progbits
