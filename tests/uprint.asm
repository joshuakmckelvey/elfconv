	.option nopic
    .data
    .section	.rodata
	.align	2
.LC0:
	.string	"Hallo Erde!"

	.text
	.align	2
	.globl	main
    .globl _start
	.type	main, @function
_start: 			# Initialize LCD, call user level program
	li t2, 0x80000000 # LCD Control/Data register location
	li t4, 0x00	# LCD Control set: ctrl out, clk low

	sb   t4, 1(t2) 	# Init LCD Control to clk low

	li   t0, 0x8038 # Sets 8 bit 2 line display
	sh   t0, 0(t2) 	# Store char in lcd data reg, LCD clock high
	sb   t4, 1(t2) 	# LCD clock low
	li   t0, 0x800e # Turn on display/cursor
	sh   t0, 0(t2) 	# Store char in lcd data reg, LCD clock high
	sb   t4, 1(t2) 	# LCD clock low
	li   t0, 0x8006 # Mode:inc and shift cursor right
	sh   t0, 0(t2) 	# Store char in lcd data reg, LCD clock high
	sb   t4, 1(t2) 	# LCD clock low

	la t0, tsr	# mtvec offset
	csrw mtvec, t0	# Sets mtvec to tsr address
	
	li t0, 0x104
    li t1, 0x80001000
	sw t0, 0x0(t1)	    # Sets mtime_cmp to 260 cycles
	sw zero, 0x4(t1)	# mtime_cmp high

	csrsi mie, 0x8	# Enables Interrupts (MIE) in mstatus

	la t0, main	# Loads addr of main
	csrw 0x341, t0	# Sets mepc to main
	li   t0, 0x30200073 # Loads mret instruction
	sw   t0, mret0, t2   # Stores mret instruction (Self-modifying code)
mret0:
	uret		# Placeholder for mret, call user program


tsr:			# Interrupt/exception service routine
	csrr t0, 0x342	# Read mcause CSR
	srli t1, t0, 31 # Extract Interrupt/Exception bit
    li t2, 0x80000000
	sw t1, 0x0008(t2)	# Store shifted mcause into Output LEDs
	bnez  t1, isr
	li t1, 0x8	# ecall from user cause
	beq t0, t1, ecall_user
	li t1, 0xB	# ecall from machine cause
	beq t0, t1, ecall_machine
	li t1, 0x2	# Illegal instruction cause
	beq t0, t1, ill_inst
	j inv_except	# Invalid Exception

ecall_user:
	li t0, 4
	bne a0, t0, inv_call # branches if not print_s syscall
	j print_s	# print_s syscall encountered

ecall_machine:
	li t0, 4
	bne a0, t0, inv_call # branches if not print_s syscall
	j print_s	# print_s syscall encountered

isr:
    li t4, 0x80001000   # Base addr of mtime
	lw t0, 0x0008(t4)	# Loads mtime low
	lw t1, 0x000C(t4)	# Loads mtime high
	lw t2, 0x0000(t4)	# Loads mtime_cmp low
	lw t3, 0x0004(t4)	# Loads mtime_cmp high
	addi t0, t0, 0x20	# adds 32 to last time count
	sw t0, 0x0000(t4)	# Sets mtime_cmp to 32 past last interrupt
	j inv_interrupt

ill_inst: 		# Illegal instruction occured
	j ill_inst	# Infinite Loop

inv_call: 		# Invalid syscall occured
	j inv_call	# Infinite Loop

inv_except: 		# Invalid interrupt occured
	j inv_except	# Infinite Loop	

inv_interrupt: 		# Invalid interrupt occured
	j inv_interrupt	# Infinite Loop


print_s:
	addi t1, a1, 0 	# Copy address of string
	li t2, 0x80000000 # LCD Control/Data register location
	li t3, 0xc0	# LCD Control set: char out, clk high
	li t4, 0x40	# LCD Control set: char out, clk low
	lb   t0, 0(t1)  # Load char
	sb   t4, 1(t2)  # Init LCD Control to clk low
loop:
	sb   t0, 0(t2)  # Store char in lcd data reg
	sb   t3, 1(t2)  # Toggle LCD clock
	sb   t4, 1(t2)
	addi t1, t1, 1  # Next char addr
	lb   t0, 0(t1)  # Load char
	bne  zero, t0, loop	# Repeat until NULL char

	csrr t0, 0x341	# Read mepc CSR
	addi t1, t0, 4	# Adds 1 instruction offset
	csrw mepc, t1	# Writes mepc+4 to mepc
	li   t0, 0x30200073 # Loads mret instruction
	sw   t0, mret1, t2   # Stores mret instruction (Self-modifying code)
mret1:
	uret		# Placeholder for mret
	nop		# Padding for data segment
    

main:           # User level program
	li a0, 4	# Loads print_s syscall code
	la a1, .LC0 # Address of string
	ecall		# Call print_s
	li a0, 10	# Loads exit syscall code
	ecall		# Call exit
	nop		# Padding

	.size	main, .-main
	.section	.note.GNU-stack,"",@progbits
