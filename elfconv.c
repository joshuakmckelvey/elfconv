/* Converts an ELF Executable to a configurable number of Verilog Bin files */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>

#include <elf.h>

int main(int argc, char *argv[]) {

  /* Input file descriptor */
  int elf_fd = -1;

  /* Pointer to memory mapped ELF file */
  const char *elf_ptr = NULL;

  struct stat elf_stat = {0};

  Elf32_Ehdr *header;

  int split, stride, wordlen, tagbits, filesize = 0;

  /* Input/Output file name */
  char *in_name, *out_name;

  /* Input arguments */
  static struct option long_options[] = {
    {"infile",  required_argument,  NULL, 'i'},
    {"split",   required_argument,  NULL, 's'},
    {"stride",  required_argument,  NULL, 0},
    {"wordlen", required_argument,  NULL, 'w'},
    {"outfile", required_argument,  NULL, 'o'},
    {"tagbits", required_argument,  NULL, 't'},
    {"filesize",required_argument,  NULL, 'z'},
    {NULL,      0,                  NULL, 0}
  };

  int opt_index = 0;
  int c;

  while ((c = getopt_long(argc, argv, "iswot",
          long_options, &opt_index)) != -1) {

    switch (c) {
    case 0:
      // long options
      if (strncmp(long_options[opt_index].name, "stride", 7) == 0) {
        
        stride = atoi(optarg);

      } else {
        
        printf("Incorrect long option\n");

      }
      break;
    case 'i':
      in_name = optarg;
      break;
    case 's':
      split = atoi(optarg);
      break;
    case 'w':
      wordlen = atoi(optarg);
      break;
    case 'o':
      if (optarg == NULL) {

        printf("Output file is not named\n");
        exit(EXIT_FAILURE);

      }
      out_name = optarg;
      break;
    case 't':
      tagbits = atoi(optarg);
      break;
    case 'z':
      filesize = atoi(optarg);
      break;
    case '?':
      printf("%c In Argment\n", c);
      exit(EXIT_FAILURE);
      break;
    default:
      printf("Failed to match arg: %c\n", c);
      exit(EXIT_FAILURE);
      break;
    }

  }


  // Debug info
  //printf("%s, %d, %d, %d, %s\n", in_name, split, stride, wordlen, out_name);

  /* ELF File Code */

  elf_fd = open(in_name, O_RDONLY);

  if (elf_fd < 0) {

    printf("Could not open file: %s\n", in_name);
    exit(EXIT_FAILURE);

  }

  /* Get stats for file */
  if (fstat(elf_fd, &elf_stat) != 0) {

    printf("Could not obtain stats for file: %s\n", in_name);
    exit(EXIT_FAILURE);

  }

  /* Load ELF file into memory */
  elf_ptr = mmap(NULL, elf_stat.st_size, PROT_READ, MAP_PRIVATE, elf_fd, 0);

  if (elf_ptr == MAP_FAILED) {

    printf("Map error for file: %s\n", in_name);
    exit(EXIT_FAILURE);

  }

  /* Makes header an alias for the ELF header */
  header = (Elf32_Ehdr *) elf_ptr;

  if (header->e_ident[0] != 0x7f ||
      header->e_ident[1] != 'E' ||
      header->e_ident[2] != 'L' ||
      header->e_ident[3] != 'F') {
    
    printf("ERROR: %s is not a ELF file\n", in_name);
    exit(EXIT_FAILURE);

  }

  if (header->e_type != ET_EXEC) {
    
    printf("ERROR: %s is not an executable ELF file\n", in_name);
    exit(EXIT_FAILURE);

  }

  /* Debug info
  printf("Text entry point: %x\n", header->e_entry);
  printf("Number of program headers: %x\n", header->e_phnum);
  */

  if (header->e_phoff == 0) {

    printf("ERROR: %s has no program header\n", in_name);
    exit(EXIT_FAILURE);

  }

  /* Assumes that first header is text, second is data */
  Elf32_Phdr* text_pheader = (Elf32_Phdr*) &elf_ptr[header->e_phoff];
  Elf32_Phdr* data_pheader = (Elf32_Phdr*) &elf_ptr[header->e_phoff +
                                                    header->e_phentsize];

  /* Text segment */
  const char* text_ptr = elf_ptr + text_pheader->p_offset;

  /* Data segment */
  const char* data_ptr = elf_ptr + data_pheader->p_offset;

  int count = 0;
    
  /* Debug info
  printf("Text Segment:\n");
  for (const char* p = text_ptr; p < text_ptr + text_pheader->p_filesz; p++) {
    
    printf("%02hhX ", *p);

    count++;

    if (count % 4 == 0) {
      
      printf("\n");

    }

  }
  printf("\n");

  count = 0;
    
  printf("Data Segment:\n");
  for (const char* p = data_ptr; p < data_ptr + data_pheader->p_filesz; p++) {
    
    printf("%02hhX ", *p);

    count++;

    if (count % 4 == 0) {
      
      printf("\n");

    }

  }
  printf("\n");
  */

  FILE **text_file = malloc(sizeof(text_file)*stride);

  /* TODO: free */
  char **text_str = malloc(sizeof(text_str)*stride);

  /* Allocates data_file even if unified */
  FILE **data_file = malloc(sizeof(text_file)*stride);

  char **data_str;

  /* Tag/Valid/Dirty files */
  FILE *text_tag_file;
  FILE *text_val_file;
  FILE *text_dty_file;

  char *text_tag_str;
  char *text_val_str;
  char *text_dty_str;

  FILE *data_tag_file;
  FILE *data_val_file;
  FILE *data_dty_file;

  char *data_tag_str;
  char *data_val_str;
  char *data_dty_str;

  /* Only allocates data file string if split */
  if (split) {
    

    data_str = malloc(stride*sizeof(data_str));

    if (data_file == NULL || data_str == NULL) {

      printf("ERROR: Malloc failure\n");
      exit(EXIT_FAILURE);

    }


  }

  if (text_file == NULL || text_str == NULL || data_file == NULL) {

    printf("ERROR: Malloc failure\n");
    exit(EXIT_FAILURE);

  }



  /* Creates the output file(s) */
  for (int i = 0; i < stride; i++) {

    char temp_str[9];

    if (split) {
    
      if (stride == 1) {

        text_str[0] = malloc(strlen(out_name) + sizeof("i.dat"));
        strcpy(text_str[0], out_name);
        strcat(text_str[0], "i.dat");

        text_file[0] = fopen(text_str[0], "w");

        data_str[0] = malloc(strlen(out_name) + sizeof("d.dat"));
        strcpy(data_str[0], out_name);
        strcat(data_str[0], "d.dat");

        data_file[0] = fopen(data_str[0], "w");

      } else {

        text_str[i] = malloc(strlen(out_name) + sizeof("i_XX.dat"));
        strcpy(text_str[i], out_name);
        sprintf(temp_str, "i_%d.dat", i);
        strcat(text_str[i], temp_str);

        text_file[i] = fopen(text_str[i],"w");

        data_str[i] = malloc(strlen(out_name) + sizeof("d_XX.dat"));
        strcpy(data_str[i], out_name);
        sprintf(temp_str, "d_%d.dat", i);
        strcat(data_str[i], temp_str);

        data_file[i] = fopen(data_str[i],"w");

      }

      if (tagbits) {

        /* Text Tag File */
        text_tag_str = malloc(strlen(out_name) + sizeof("i_tag.dat"));
        strcpy(text_tag_str, out_name);
        strcat(text_tag_str, "i_tag.dat");

        text_tag_file = fopen(text_tag_str, "w");

        /* Text Valid File */
        text_val_str = malloc(strlen(out_name) + sizeof("i_val.dat"));
        strcpy(text_val_str, out_name);
        strcat(text_val_str, "i_val.dat");

        text_val_file = fopen(text_val_str, "w");

        /* Text Dirty File */
        text_dty_str = malloc(strlen(out_name) + sizeof("i_dty.dat"));
        strcpy(text_dty_str, out_name);
        strcat(text_dty_str, "i_dty.dat");

        text_dty_file = fopen(text_dty_str, "w");

        /* Data Tag File */
        data_tag_str = malloc(strlen(out_name) + sizeof("d_tag.dat"));
        strcpy(data_tag_str, out_name);
        strcat(data_tag_str, "d_tag.dat");

        data_tag_file = fopen(data_tag_str, "w");

        /* Data Valid File */
        data_val_str = malloc(strlen(out_name) + sizeof("d_val.dat"));
        strcpy(data_val_str, out_name);
        strcat(data_val_str, "d_val.dat");

        data_val_file = fopen(data_val_str, "w");

        /* Data Dirty File */
        data_dty_str = malloc(strlen(out_name) + sizeof("d_dty.dat"));
        strcpy(data_dty_str, out_name);
        strcat(data_dty_str, "d_dty.dat");

        data_dty_file = fopen(data_dty_str, "w");

      }

    } else {
      
      if (stride == 1) {

        text_str[0] = malloc(strlen(out_name) + sizeof(".dat"));
        strcpy(text_str[0], out_name);
        strcat(text_str[0], ".dat");

      } else {

        text_str[i] = malloc(strlen(out_name) + sizeof("_XX.dat"));
        strcpy(text_str[i], out_name);
        sprintf(temp_str, "_%d.dat", i);
        strcat(text_str[i], temp_str);

      }

      text_file[i] = fopen(text_str[i],"w");

      data_file[i] = text_file[i];

      if (tagbits) {

        /* Text Tag File */
        text_tag_str = malloc(strlen(out_name) + sizeof("_tag.dat"));
        strcpy(text_tag_str, out_name);
        strcat(text_tag_str, "_tag.dat");

        text_tag_file = fopen(text_tag_str, "w");

        /* Text Valid File */
        text_val_str = malloc(strlen(out_name) + sizeof("_val.dat"));
        strcpy(text_val_str, out_name);
        strcat(text_val_str, "_val.dat");

        text_val_file = fopen(text_val_str, "w");

        /* Text Dirty File */
        text_dty_str = malloc(strlen(out_name) + sizeof("_dty.dat"));
        strcpy(text_dty_str, out_name);
        strcat(text_dty_str, "_dty.dat");

        text_dty_file = fopen(text_dty_str, "w");

        /* Alias Data Tag File */
        data_tag_file = text_tag_file;

        /* Alias Data Valid File */
        data_val_file = text_val_file;

        /* Alias Data Dirty File */
        data_dty_file = text_dty_file;

      }

    }

  }

  /* Initializes count to text virtual address */
  count = text_pheader->p_vaddr;
    
  /* Outputs text segment */
  for (const char* p = text_ptr; p < text_ptr + text_pheader->p_filesz; p++) {
    
    if (wordlen == 1) {
      fprintf(text_file[count % stride],"%02hhX ", *p);
    } else if (wordlen == 4) {
      fprintf(text_file[count % stride],"%02hhX", *p);
    }

    /* Outputs tag/val/dty */
    if ((count) % 16 == 0) {
      
      fprintf(text_tag_file,"%05x\n", count>>(32-tagbits));
      fprintf(text_val_file,"1\n");
      fprintf(text_dty_file,"1\n");

    }

    count++;

    if ((count/stride) % 4 == 0 && count/stride != 0) {
      
      fprintf(text_file[count % stride],"\n");

    }

  }

  /* Offsets data segment if unified */
  if (!split) {

    /* Fills out space between text and data segments with zero */
    while ((unsigned int) count < data_pheader->p_vaddr) {
      
      if (wordlen == 1) {
        fprintf(text_file[count % stride],"00 ");
      } else if (wordlen == 4) {
        fprintf(text_file[count % stride],"00");
      }

      /* Outputs tag/val/dty */
      if ((count) % 16 == 0) {
        
        fprintf(text_tag_file,"%05x\n", count>>(32-tagbits));
        fprintf(text_val_file,"0\n");
        fprintf(text_dty_file,"0\n");

      }

      count++;

      if ((count/stride) % 4 == 0) {
        
        fprintf(text_file[count % stride],"\n");

      }

    }

  }

  // Reset count to address of data section
  count = data_pheader->p_vaddr;
    
  /* Outputs data segment */
  for (const char* p = data_ptr; p < data_ptr + data_pheader->p_filesz; p++) {
    
    if (wordlen == 1) {
      fprintf(data_file[count % stride],"%02hhX ", *p);
    } else if (wordlen == 4) {
      fprintf(data_file[count % stride],"%02hhX", *p);
    }

    /* Outputs tag/val/dty */
    if ((count) % 16 == 0) {
      
      fprintf(data_tag_file,"%05x\n", count>>(32-tagbits));
      fprintf(data_val_file,"1\n");
      fprintf(data_dty_file,"1\n");

    }

    count++;

    if ((count/stride) % 4 == 0 && count/stride != 0) {
      
      fprintf(data_file[count % stride],"\n");

    }

  }

  /* Zeros out the rest of the file */
  while ((unsigned int) count < (filesize*4)) { // Filesize converted to bytes
    
    if (wordlen == 1) {
      fprintf(text_file[count % stride],"00 ");
    } else if (wordlen == 4) {
      fprintf(text_file[count % stride],"00");
    }

    /* Outputs tag/val/dty */
    if ((count) % 16 == 0) {
      
      fprintf(text_tag_file,"%05x\n", count>>(32-tagbits));
      fprintf(text_val_file,"0\n");
      fprintf(text_dty_file,"0\n");

    }

    count++;

    if ((count/stride) % 4 == 0) {
      
      fprintf(text_file[count % stride],"\n");

    }

  }

  /* Closes files */
  for (int i = 0; i < stride; i++) {
    
    fprintf(text_file[i], "\n");
    fclose(text_file[i]);

    if (split) {

      fprintf(data_file[i], "\n");
      fclose(data_file[i]);

    }

  }



  /* TODO: Add TLB and cache mapping outputs */

  close(elf_fd);

  /* munmap would occur here */

  return 0;

}
